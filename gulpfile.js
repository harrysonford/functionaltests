require('./.env');

var gulp = require('gulp');
const newman = require('newman'); 

gulp.task('party', async function () {
	newman.run({
		collection: require('./collection.json'),
		folder: 'party',
		environment: require('./environment.json'),
		reporters: 'cli'
	}, function (err) {
		if (err) { throw err; }
		console.log('collection run complete!');
	});

	return Promise.resolve('done');
})